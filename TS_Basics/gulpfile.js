	
/*npm install gulpjs/gulp.git#4.0 */
// requre gulp
var gulp = require('gulp');
// Module to remove existing compiled files
var del = require('del');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
// require gulp-typescript module to compile typescript
var ts = require('gulp-typescript');
// create project ts config project in order to use tsconfig option.json file
var tsProject = ts.createProject("tsconfig.json");

gulp.task('ts-compile', function() {
    return gulp.src("src/**/*.ts")
        .pipe(sourcemaps.init())
        //create ts project on the fly using options
        .pipe(ts({ 
            "module": "amd", // generate amd module type
            "outFile": "app.js", // amd requrires single file to output
            "allowSyntheticDefaultImports": true,
            "target": "es5",
            "sourceMap": true,
            "noImplicitAny": false,
            "typeRoots": [
                "/_build/node_modules/@types/",
            ]
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest("dist"))
        .on('error', function(err) {
            console.log(err.toString());
            this.emit('end'); //close buffer 
        });
});
// Clear dist folder
gulp.task('clean', function() {
    return del([
        '#dist/**/*' 
    ]);
});

/* Alternative build to use tsproject */
gulp.task('ts-compile-alt', function() {
    return gulp.src("src/**/*.ts")
        .pipe(sourcemaps.init())
        .pipe(tsProject())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest("dist"))
        .on('error', function(err) {
            console.log(err.toString());
            this.emit('end'); //close buffer 
        });
    });
// Move libs 
gulp.task('add-lib', function() {
    return gulp.src("lib/**/*")
    .pipe(gulp.dest("dist/lib"));
});
// Move index and main.js 
gulp.task('styles', function(){
    return gulp.src("src/css/**/*.scss")
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest("dist/css/"));
});
// Move index and main.js 
gulp.task('add-core', function(){
    return gulp.src("src/app/*")
    .pipe(gulp.dest("dist"));
});

//watch all folders
gulp.task('watch',  gulp.series('clean', 'add-core', 'styles','add-lib', 'ts-compile-alt', function() {
    gulp.watch('src/**/*', gulp.series('add-core', 'styles', 'add-lib', 'ts-compile-alt'));
}));