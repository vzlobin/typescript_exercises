/// <reference path="../../../node_modules/@types/jqueryui/index.d.ts"/>
// Point to Jquery defined in main.js since this namespace will be resolved in it
import $ = require("jquery");
// Import jquery-ui module from requireJS definition
import "jquery-ui";
// Import static modules 
import {ModuleOne} from "./moduleOne";
import {ModuleTwo} from "../deps/moduleTwo";

export class PrintModule {
    printMsg(subject: string): void {
        console.log("Sy hi from ", subject);
    }
    addText(): void {
        // Testing jquery
        $("#app-div").append("<div>Jquery Appened text</div>");
        // Testing jquery-ui
        $("#tabs").tabs();
    }
    useModuleOneSayHello(): void {
        ModuleOne.sayHello();
    }
    printModuleTwo(): void {
        var m2 = new ModuleTwo({name: "module two"});
        m2.sayHi();
        m2.setName("module two and half");
        m2.sayHi();
    }
}