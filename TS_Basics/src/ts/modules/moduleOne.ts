
export class ModuleOne {
    static sayHello() {
        console.log("Hello from Module1!");
    }

    static sayHelloTo(who: string) {
        console.log("Hello " + who.trim() + ". This is Module1");
    }
}