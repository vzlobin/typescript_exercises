import * as PrintMod from "./modules/printModule";
// ecport App module
export class App {
    public init(): void {
        // Initialize constructor
        let print = new PrintMod.PrintModule();

        print.printMsg("Slav");
        print.addText();

        print.useModuleOneSayHello();
        print.printModuleTwo();
        // Hide loading text
        this.toggeLoading();
    }

    public toggeLoading(): void {
        $("#app-container").toggleClass("loading");
    }
    
}