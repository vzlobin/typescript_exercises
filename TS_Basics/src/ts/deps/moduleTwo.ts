export class ModuleTwo {
    private name: string;

    constructor(options: {name:string}) {
        this.name = options.name;
    }

    public sayHi():void {
        console.log(this.name +" sayed hi");
    }

    public sayHelloTo(who: string): void {
        console.log("Hello " + who.trim() + ". This is Module two");
    }

    public setName(name:string):void {
        this.name = name;
    } 
}