requirejs.config({
    baseUrl: './',
    paths: {
        'css': 'lib/css', //define path to require.js css plugin
        'App': 'app',
        'jquery': 'lib/jquery',
        'jquery-ui': 'lib/jquery_ui/jquery-ui'
    },
    shim: {
        "jquery-ui": {
            exports: "$",
            deps: ['jquery']
        }
    }
});
// Require css modules before loading
// Css base defaults to lib directory
define(["css!lib/jquery_ui/jquery-ui.css",
"css!lib/jquery_ui/jquery-ui.structure.css",
"css!lib/jquery_ui/jquery-ui.theme.css",
"css!../css/styles.css" //depending on server config this may change
    ], function(){
    
    requirejs(['App'], function(MyApp) {
        console.log("Starting app");
        // Expose app module under namespace MyApp
        var app = new MyApp.App();
        // Initialize application
        app.init();
    });
});