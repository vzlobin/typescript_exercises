	
/*npm install gulpjs/gulp.git#4.0 */
// requre gulp
var gulp = require('gulp');
// Module to remove existing compiled files
var del = require('del');
var sass = require('gulp-sass');
var babel = require('gulp-babel');
var handlebars = require('gulp-handlebars');
var wrap = require('gulp-wrap');
var declare = require('gulp-declare');
var concat = require('gulp-concat');

var sourcemaps = require('gulp-sourcemaps');
// require gulp-typescript module to compile typescript
var ts = require('gulp-typescript');
// create project ts config project in order to use tsconfig option.json file
var tsProject = ts.createProject("tsconfig.json");

// Clear dist folder
gulp.task('clean', function() {
    return del([
        'dist/**/*' 
    ]);
});

/* Alternative build to use tsproject */
gulp.task('ts-compile-alt', function() {
    return gulp.src("src/**/*.ts")
        .pipe(sourcemaps.init())
        .pipe(tsProject())
        .pipe(babel())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest("dist"))
        .on('error', function(err) {
            console.log(err.toString());
            this.emit('end'); //close buffer 
        });
    });
// Move libs 
gulp.task('add-lib', function() {
    return gulp.src("lib/**/*")
    .pipe(gulp.dest("dist/lib"));
});
// Move index and main.js 
gulp.task('styles', function(){
    return gulp.src("src/css/**/*.scss")
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest("dist/css/"));
});
// Move index and main.js 
gulp.task('add-core', function(){
    return gulp.src("src/app/*")
    .pipe(gulp.dest("dist"));
});

gulp.task('compile-templates', function() {
    return gulp.src('src/rawTemplates/*.hbs')
        //pass local version of the handlebars
        .pipe(handlebars(
            {handlebars: require('handlebars')}
        ))
        // Define templates as AMD modules
        .pipe(wrap('Handlebars.template(<%= contents %>)'))
        .pipe(declare({
            root: "templates",
            noRedeclare: true
            }))
        .pipe(concat('templates.js'))
        .pipe(wrap("define(['handlebars'], function (Handlebars){"
        +"Handlebars = Handlebars['default']; "
        +"var templates = Handlebars.templates || {}; "
        +" <%= contents %> return templates;});"))
        .pipe(gulp.dest('src/templates'));
});

gulp.task('templates', gulp.series('compile-templates', 
    function(){
        return gulp.src("src/templates/*")
        .pipe(gulp.dest("dist/templates"));
    }
));


//watch all folders
gulp.task('watch',  gulp.series('clean', 'add-core', 'templates', 'styles','add-lib', 'ts-compile-alt', function() {
    gulp.watch(['src/**/*.ts','src/app/*.html', 'src/app/*.js', 'src/**/*.hbs', 'src/**/*.scss'], 
    gulp.series('clean', 'templates', 'add-core','styles', 'add-lib', 'ts-compile-alt'));
}));