requirejs.config({
    baseUrl: './',
    paths: {
        'css': 'lib/css', //define path to require.js css plugin
        'App': 'app',
        'jquery': 'lib/jquery',
        'underscore': 'lib/underscore',
        'backbone': 'lib/backbone',
        'lodash': 'lib/lodash',
        'handlebars': 'lib/handlebars',
        'jquery-ui': 'lib/jquery_ui/jquery-ui',
        'Templates': 'templates/templates'
    },
    shim: {
        'jquery-ui': {
            exports: '$',
            deps: ['jquery']
        },
        'underscore': {
            exports: '_'
        },
        'backbone': {
            deps: ['underscore', 'jquery']
        }
    }
});
// Require css modules before loading
// Css base defaults to lib directory
define(["css!lib/jquery_ui/jquery-ui.css",
"css!lib/jquery_ui/jquery-ui.structure.css",
"css!lib/jquery_ui/jquery-ui.theme.css",
"css!../css/styles.css" //depending on server config this may change
    ], function(){
    
    requirejs(['App', 'backbone'], function(MyApp, Backbone) {
        // Must use App as medium for module loading 
        // since subfolders such as views are resoled at the run time  
        var app = new MyApp.App();
        app.init();
    });
});