//module from node_modules @type definition
import _ = require("lodash");
import Backbone = require('backbone');

interface Options { 
    name?: string, 
    age?: number,
    gender?: string 
}
export class Person extends Backbone.Model{
    initialize(options: Options) {
        this.set("name", _.isString(options.name) ? options.name : "");
        this.set("age", (options.age || 0));
        this.set("gender", (options.gender || ""));
    }
    public greet() {
        return "Hi my name is "+this.get("name")+", i am " + this.get("age"); 
    }
}