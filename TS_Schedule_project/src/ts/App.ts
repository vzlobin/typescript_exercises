import Backbone = require('backbone');

import {AppModel} from "./models/AppModel";
import {AppView} from "./views/AppView";
export class App {
    public init(): void {
        // Initialize constructor
        // Hide loading text
        var appModel = new AppModel();
        var app = new AppView({el: '#app-div', model: appModel});       
    }
}