import Backbone = require('backbone');

import {Person} from "../models/Person";
import {People} from "../collections/People";
import {Actions} from "../models/Actions";

import {ButtonsView} from "./ButtonsView";
import {PersonView} from "./PersonView";
import {PersonFormView} from "./PersonFormView";


import _ = require("lodash");
// ecport App module
export class AppView extends Backbone.View<Backbone.Model> {
    people: People;
    personView: PersonView;
    actions: Actions;
    buttonsView: ButtonsView;
    bus: Object;
    appRouter: Backbone.Router;
    constructor(options: any) {
        super(options);
    }
    // Initialize constructor
    public initialize(options?: any): void {
        this.bus = {};
        _.extend(this.bus, Backbone.Events);

        this.initViews();
        this.toggeLoading();
    }

    private initViews(){
        // this.people = new People([
        //     new Person({name: "James", age:24, gender:"male"}),
        //     new Person({name: "Jane", age:45, gender:"female"})
        // ]);

        // this.personView = new PersonView({el: "#p-table", model: this.people, 
        //     bus: this.bus});
        this.actions = new Actions({});

        this.buttonsView = new ButtonsView({el: "#action-selection", model: this.actions,
            bus: this.bus});
            
    }
    private initRouts() {
        var AppRouter = Backbone.Router.extend({
            routes: {
                "preview": "viewAll",
                "addPerson": "addPerson",
                "editPerson/:personId": "editPerson"
            },
            viewAll: function() {
                var view = new PersonView({el: "#p-table", model: this.people, 
                    bus: this.bus});
                view.render();
            },
            addPerson: function() {
                // var view = new PersonFormView({el: "#", model: this.people, 
                // bus: this.bus});
                // view.render();
            }
        });

        this.appRouter = new AppRouter();
        Backbone.history.start();
    }

    private toggeLoading(): void {
        $("#app-container").toggleClass("loading");
    }
    
}
