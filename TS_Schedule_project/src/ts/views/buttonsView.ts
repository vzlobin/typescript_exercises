/// <reference types = "jqueryui"/>
/// <reference types = "backbone"/>
// Point to Jquery defined in main.js since this namespace will 
//  be resolved in it
import $ = require("jquery");
import "jquery-ui"; 
import _ = require("lodash");
import Backbone = require('backbone');
import * as Templates from "../../templates/templates";

export class ButtonsView extends Backbone.View<Backbone.Model> {
    template: (...data: any[]) => string;
    bus: Object; //Event bus passing events between views
    // Using getter functionality of the Typescript
    // stackoverflow.com/questions/21546652/running-backbonejs-events-in-typescript
    events(): Backbone.EventsHash {
        return {
            'click #add-person-btn':'onAddClick',
            'click #edit-person-btn':'onEditClick',
            'click #remove-person-btn': 'onRemoveClick',
            'click #view-all-btn': 'onViewAllClick'
        }
    }
    constructor(options: any) {
        super(options);
    }

    initialize(options?: any): void{
        this.template = Templates.buttons;
        this.bus = options.bus;
        this.render();
    }
    public render(): any {
        this.$el.html(this.template(this.model.toJSON()));
        $("#action-selection").buttonset();
        return this;
    }

    protected onAddClick(): void {
        console.log("add btn");
    }
    protected onEditClick(): void {
        console.log("edit btn");
        
    }
    protected onRemoveClick(): void {
        console.log("remove btn");
        
    }
    protected onViewAllClick(): void {
        console.log("viewall btn");

    }
}