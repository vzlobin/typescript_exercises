
/// <reference types = "jqueryui"/>
// Point to Jquery defined in main.js since this namespace will 
//  be resolved in it
import $ = require("jquery");
import "jquery-ui"; 
import _ = require("lodash");
import Backbone = require('backbone');

import {Person} from "../models/Person";
import {People} from "../collections/People";

import * as Templates from "../../templates/templates";

// Observer provides intermodel communication
export class PersonFormView extends Backbone.View<Backbone.Model | any> {
    template: (...data: any[]) => string;
    bus: Object;

    constructor(people: People) {
        super({
            className: "bbform",
            model: people
        });
    }
    public initialize(options) {
        this.template = Templates.addForm;
        this.bus = options.bus;
    }
    public render(): any {
        return this;
    }


}