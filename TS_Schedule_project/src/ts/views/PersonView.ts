import Backbone = require('backbone');
import _ = require('lodash');
import * as Templates from "../../templates/templates";

export class PersonView extends Backbone.View<Backbone.Model | any> {
    template: (...data: any[]) => string;
    bus: Object; //Event bust passing events between views

    constructor(options: any) {
        super(options);
    }
    initialize(options?: any): void{
        this.template = Templates.personForm;
        this.bus = options.bus;
    }
    public render(): any {
        var self = this;
        this.$el.html();
        this.model.each( model => {
            self.$el.append(self.template(model.toJSON()));
        }, this)
        return this;
    }
}