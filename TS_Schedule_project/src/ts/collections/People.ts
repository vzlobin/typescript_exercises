import Backbone = require('backbone');
import {Person} from "../models/Person";

export class People extends Backbone.Collection<Backbone.Model> {
    model = Person;
    constructor(options?) {
        super(options);
    };
}