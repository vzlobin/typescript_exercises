define(['handlebars'], function (Handlebars){Handlebars = Handlebars['default']; var templates = Handlebars.templates || {};  templates["addForm"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    return "selected";
},"3":function(container,depth0,helpers,partials,data) {
    return "  <button id=\"add-person-btn\">Add New </button>\r\n";
},"5":function(container,depth0,helpers,partials,data) {
    return "  <button id=\"edit-person-btn\">Edit Existing</button>>\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<form>\r\n  <input type=\"text\" name=\"pname\" id=\"person-name\" value=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" placeholder=\"Enter Full Name\">\r\n  <label for=\"person-name\">Name:</label>\r\n  <input type=\"number\" name=\"age\" id=\"person-age\" value=\""
    + alias4(((helper = (helper = helpers.age || (depth0 != null ? depth0.age : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"age","hash":{},"data":data}) : helper)))
    + "\" placeholder=\"Enter age\" min=\"16\">\r\n  <label for=\"person-age\"></label>\r\n  <select id=\"person-gender\">\r\n    <option value=\"male\" "
    + ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || alias2).call(alias1,(depth0 != null ? depth0.gender : depth0),"male",{"name":"if_eq","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ">male</option>\r\n    <option value=\"female\" "
    + ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || alias2).call(alias1,(depth0 != null ? depth0.gender : depth0),"female",{"name":"if_eq","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ">female</option>\r\n  </select>\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.addNew : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.program(5, data, 0),"data":data})) != null ? stack1 : "")
    + "</form>";
},"useData":true});
templates["buttons"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<input type=\"radio\" id=\"add-person-btn\" name=\"action buttons\" class=\"selected\">\r\n<label for=\"add-person-btn\">Add New Person</label>\r\n<input type=\"radio\" id=\"edit-person-btn\" name=\"action buttons\">\r\n<label for=\"edit-person-btn\">Edit Person</label>\r\n<input type=\"radio\" id=\"remove-person-btn\" name=\"action buttons\">\r\n<label for=\"remove-person-btn\">Remove Person</label>\r\n<input type=\"radio\" id=\"view-all-btn\" name=\"action buttons\" checked>\r\n<label for=\"view-all-btn\">View All</label>";
},"useData":true});
templates["personForm"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<tr>\r\n    <td class=\"p-name\">"
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "</td>\r\n    <td class=\"p-age\">"
    + alias4(((helper = (helper = helpers.age || (depth0 != null ? depth0.age : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"age","hash":{},"data":data}) : helper)))
    + "</td>\r\n    <td class=\"p-gender\">"
    + alias4(((helper = (helper = helpers.gender || (depth0 != null ? depth0.gender : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"gender","hash":{},"data":data}) : helper)))
    + "</td>\r\n</tr>";
},"useData":true}); return templates;});